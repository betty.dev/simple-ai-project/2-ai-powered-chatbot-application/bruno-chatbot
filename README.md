# ChatBot with Nuxt, TypeScript and the OpenAI Assistants API

# An AI-powered customer support ChatBot!

Welcome to the HealthBerry AI Bot repository! This project aims to assist users and developers of the HealthBerry platform by providing an intelligent chatbot interface powered by OpenAI's ChatGPT model.Whether you're seeking information about specific features or exploring the platform's capabilities, the AI Bot is here to help.

## Features

- **Natural Language Understanding**: The AI Bot is trained on a diverse dataset to understand and respond to natural language queries effectively.
- **Real-time Assistance**: Get instant responses to your questions and concerns, ensuring a seamless user experience.
- **Backend Integration**: Utilizing the OpenAI SDK, the bot can perform backend requests to fetch relevant data and information.
- **TypeScript and Vue**: The project is developed using TypeScript for backend logic and Vue.js for the frontend interface, ensuring scalability and maintainability.

## Usage

To interact with the HealthBerry AI Bot, simply access the chat interface provided on the HealthBerry platform. Type your questions or concerns, and the bot will provide timely responses based on its training and backend integration.

## Development

Creating and managing state within Nuxt applications
sending and handling HTTP requests effectively in a full-stack environment, connecting front-end interactions to back-end services
Learn to use TailwindCSS for efficient and responsive styling, creating visually appealing interfaces that enhance user experience

## Demo

[Customer Support AI demo](https://gitlab.com/betty.dev/simple-ai-project/2-ai-powered-chatbot-application/bruno-chatbot/-/blob/master/public/demo.m4v?ref_type=heads)

<video width="320" height="240" controls>
  <source src="https://gitlab.com/betty.dev/simple-ai-project/2-ai-powered-chatbot-application/bruno-chatbot/-/raw/master/public/demo.m4v?inline=false" type="video/mp4">
  Your browser does not support the video tag.
</video>
